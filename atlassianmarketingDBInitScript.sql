-- -----------------------------------------------------
-- Schema atlassianmarketinginterview
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `atlassianmarketinginterview`;
CREATE SCHEMA IF NOT EXISTS `atlassianmarketinginterview` DEFAULT CHARACTER SET utf8 ;
USE `atlassianmarketinginterview` ;

-- -----------------------------------------------------
-- Table `atlassianmarketinginterview`.`account`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `atlassianmarketinginterview`.`account`;
CREATE TABLE IF NOT EXISTS `atlassianmarketinginterview`.`account` (
`account_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
`company_name` VARCHAR(50) DEFAULT NULL,
`address_line_1` VARCHAR(50) DEFAULT NULL,
`address_line_2` VARCHAR(50) DEFAULT NULL,
`city` VARCHAR(50) DEFAULT NULL,
`state` VARCHAR(2) DEFAULT NULL,
`postal_code` VARCHAR(10) DEFAULT NULL,
`country` VARCHAR(50) DEFAULT NULL
);

-- -----------------------------------------------------
-- Insert data for table `atlassianmarketinginterview`.`account`
-- -----------------------------------------------------
LOCK TABLES `atlassianmarketinginterview`.`account` WRITE;
INSERT INTO `atlassianmarketinginterview`.`account` VALUES (1,'Atlassian','301 E Evelyn Ave',null,'Mountain View','CA','94041','United States'),(2,'Apple','One Apple Park Way',null,'Cupertino','CA','95014','United States');
UNLOCK TABLES;

-- -----------------------------------------------------
-- Table `atlassianmarketinginterview`.`contact`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `atlassianmarketinginterview`.`contact`;
CREATE TABLE IF NOT EXISTS `atlassianmarketinginterview`.`contact` (
`contact_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
`name` VARCHAR(50) DEFAULT NULL,
`email` VARCHAR(50) DEFAULT NULL,
`address_line_1` VARCHAR(50) DEFAULT NULL,
`address_line_2` VARCHAR(50) DEFAULT NULL,
`city` VARCHAR(50) DEFAULT NULL,
`state` VARCHAR(2) DEFAULT NULL,
`postal_code` VARCHAR(10) DEFAULT NULL,
`country` VARCHAR(50) DEFAULT NULL,
`account_id` INT NULL REFERENCES `account`(`account_id`)
);

-- -----------------------------------------------------
-- Insert data for table `atlassianmarketinginterview`.`contact`
-- -----------------------------------------------------
LOCK TABLES `atlassianmarketinginterview`.`contact` WRITE;
INSERT INTO `atlassianmarketinginterview`.`contact` VALUES 
(1,'Lily Hamilton','lily.hamilton@atlassianmarketinginterview.com','5 San Pablo Lane','Suite 7','Plymouth','MA','02360','United States',1),
(2,'Ryan Ferguson','ryan.ferguson@atlassianmarketinginterview.com','261 Locust Street',null,'Noblesville','IN','46060','United States',2),
(3,'Luke Wallace','luke.wallace@atlassianmarketinginterview.com','266 Strawberry St.',null,'West Des Moines','IA','50265','United States',1),
(4,'Robert Dickens','robert.dickens@atlassianmarketinginterview.com','7443 Cambridge Ave.',null,'Sandusky','OH','44870','United States',null);
UNLOCK TABLES;