package com.atlassian.marketplace.api.repository;

import com.atlassian.marketplace.api.model.Contact;
import org.springframework.data.repository.CrudRepository;

public interface ContactRepository extends CrudRepository<Contact, Integer> {
}
