package com.atlassian.marketplace.api.repository;

import com.atlassian.marketplace.api.model.Account;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account, Integer> {
}
