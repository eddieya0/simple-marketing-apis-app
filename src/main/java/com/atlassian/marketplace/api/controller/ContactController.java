package com.atlassian.marketplace.api.controller;

import com.atlassian.marketplace.api.exception.ResourceNotFoundException;
import com.atlassian.marketplace.api.model.Account;
import com.atlassian.marketplace.api.model.Contact;
import com.atlassian.marketplace.api.repository.AccountRepository;
import com.atlassian.marketplace.api.repository.ContactRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ContactController {

    private static final Logger log = LoggerFactory.getLogger(ContactController.class);

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private AccountRepository accountRepository;

    /**
     * GET contact endpoint
     * @param id contact id
     * @return contact
     * @throws ResourceNotFoundException
     */
    @GetMapping("/contacts/{id}")
    public Contact getContactById(@PathVariable int id) throws ResourceNotFoundException {
        log.debug("Getting contact of id: {}", id);
        return contactRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Requested contact with id " + id + " is not found."));
    }

    /**
     * POST contact endpoint
     * @param contact contact to post
     * @return response
     */
    @PostMapping("/contacts")
    public ResponseEntity addContact(@RequestBody Contact contact) {
        contactRepository.save(contact);
        return ResponseEntity.ok("New contact with id " + contact.getId() + " is created.");
    }

    /**
     * Associate contact with account endpoint
     * @param id contact id
     * @param accountId account id to associate the contact with
     * @return response
     * @throws ResourceNotFoundException
     */
    @PostMapping("/contacts/{id}/associate")
    public ResponseEntity associateContactWithAccount(@PathVariable int id, @RequestParam int accountId) throws ResourceNotFoundException {
        Contact contact = contactRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Requested contact with id " + id + " is not found."));
        Account account = accountRepository.findById(accountId).orElseThrow(() -> new ResourceNotFoundException("Cannot associate contacts with account id " + accountId + " because record is not found."));
        contact.setAccount(account);
        contactRepository.save(contact);
        return ResponseEntity.ok("Contact with id " + contact.getId() + " is associated with account with id " + account.getId() + ".");
    }

    /**
     * PUT contact endpoint
     * @param contact contact to put
     * @param id contact id
     * @return response
     * @throws ResourceNotFoundException
     */
    @PutMapping("/contacts/{id}")
    public ResponseEntity updateContact(@RequestBody Contact contact, @PathVariable int id) throws ResourceNotFoundException {
        contactRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Cannot update contact with id " + id + " because record is not found."));
        contact.setId(id);
        contactRepository.save(contact);
        return ResponseEntity.ok("Contact with id " + id + " is updated.");
    }

}
