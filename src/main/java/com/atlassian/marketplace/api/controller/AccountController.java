package com.atlassian.marketplace.api.controller;

import com.atlassian.marketplace.api.exception.ResourceNotFoundException;
import com.atlassian.marketplace.api.model.Account;
import com.atlassian.marketplace.api.model.Contact;
import com.atlassian.marketplace.api.repository.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AccountController {

    private static final Logger log = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private AccountRepository accountRepository;

    /**
     * GET all accounts endpoint
     * @return list of accounts found in DB
     */
    @GetMapping("/accounts")
    public List<Account> getAllAccounts() {
        log.debug("Getting all accounts");
        return (List<Account>) accountRepository.findAll();
    }

    /**
     * GET all contacts for account endpoint
     * @param id account id
     * @return list of contacts for the account
     * @throws ResourceNotFoundException
     */
    @GetMapping("/accounts/{id}/contacts")
    public List<Contact> getAllContactsForAccountById(@PathVariable int id) throws ResourceNotFoundException {
        Account account = accountRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Requested account with id " + id + " is not found."));
        List<Contact> contacts = account.getContacts();
        return contacts;
    }

    /**
     * GET account endpoint
     * @param id account id
     * @return account
     * @throws ResourceNotFoundException
     */
    @GetMapping("/accounts/{id}")
    public Account getAccountById(@PathVariable int id) throws ResourceNotFoundException {
        return accountRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Requested account with id " + id + " is not found."));
    }

    /**
     * POST account endpoint
     * @param account account to post
     * @return response
     */
    @PostMapping("/accounts")
    public ResponseEntity addAccount(@RequestBody Account account) {
        accountRepository.save(account);
        return ResponseEntity.ok("New account with id " + account.getId() + " is created.");
    }

    /**
     * PUT account endpoint
     * @param account account to put
     * @param id account id
     * @return response
     * @throws ResourceNotFoundException
     */
    @PutMapping("/accounts/{id}")
    public ResponseEntity updateAccount(@RequestBody Account account, @PathVariable int id) throws ResourceNotFoundException {
        accountRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Cannot update account with id " + id + " because record is not found."));
        account.setId(id);
        accountRepository.save(account);
        return ResponseEntity.ok("Account with id " + id + " is updated.");
    }

}
