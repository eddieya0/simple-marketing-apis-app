package com.atlassian.marketplace.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleMarketingApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleMarketingApiApplication.class, args);
	}

}
