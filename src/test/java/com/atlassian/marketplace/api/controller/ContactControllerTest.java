package com.atlassian.marketplace.api.controller;

import com.atlassian.marketplace.api.JsonUtil;
import com.atlassian.marketplace.api.SimpleMarketingApiApplication;
import com.atlassian.marketplace.api.model.Account;
import com.atlassian.marketplace.api.model.Contact;
import com.atlassian.marketplace.api.repository.AccountRepository;
import com.atlassian.marketplace.api.repository.ContactRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SimpleMarketingApiApplication.class)
@AutoConfigureMockMvc
class ContactControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ContactRepository contactRepository;

    @MockBean
    private AccountRepository accountRepository;

    @Test
    void testGetContactById() throws Exception {
        Contact contact1 = new Contact(1,"Lily Hamilton","lily.hamilton@atlassianmarketinginterview.com","5 San Pablo Lane","Suite 7","Plymouth","MA","02360","United States");
        when(contactRepository.findById(anyInt())).thenReturn(Optional.of(contact1));
        mvc.perform(get("/contacts/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", is("Lily Hamilton")));
    }

    @Test
    void testAddContact() throws Exception {
        Contact contact1 = new Contact(1,"Lily Hamilton","lily.hamilton@atlassianmarketinginterview.com","5 San Pablo Lane","Suite 7","Plymouth","MA","02360","United States");
        when(contactRepository.save(any(Contact.class))).thenReturn(contact1);
        MockHttpServletResponse response = mvc.perform(post("/contacts").contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJson(contact1))).andReturn().getResponse();
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testAssociateContactWithAccount() throws Exception {
        Contact contact1 = new Contact(1,"Lily Hamilton","lily.hamilton@atlassianmarketinginterview.com","5 San Pablo Lane","Suite 7","Plymouth","MA","02360","United States");
        Account account1 = new Account(1,"Atlassian","301 E Evelyn Ave",null,"Mountain View","CA","94041","United States");
        contact1.setAccount(account1);
        when(contactRepository.findById(anyInt())).thenReturn(Optional.of(contact1));
        when(accountRepository.findById(anyInt())).thenReturn(Optional.of(account1));
        MockHttpServletResponse response = mvc.perform(post("/contacts/1/associate?accountId=1")
                .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testUpdateContact() throws Exception {
        Contact contact1 = new Contact(1,"Lily Hamilton","lily.hamilton@atlassianmarketinginterview.com","5 San Pablo Lane","Suite 7","Plymouth","MA","02360","United States");
        when(contactRepository.findById(anyInt())).thenReturn(Optional.of(contact1));
        when(contactRepository.save(any(Contact.class))).thenReturn(contact1);
        MockHttpServletResponse response = mvc.perform(put("/contacts/1").contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJson(contact1))).andReturn().getResponse();
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }
}