package com.atlassian.marketplace.api.controller;

import com.atlassian.marketplace.api.JsonUtil;
import com.atlassian.marketplace.api.SimpleMarketingApiApplication;
import com.atlassian.marketplace.api.model.Account;
import com.atlassian.marketplace.api.model.Contact;
import com.atlassian.marketplace.api.repository.AccountRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SimpleMarketingApiApplication.class)
@AutoConfigureMockMvc
class AccountControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AccountRepository repository;
    
    private List<Account> accounts = new ArrayList<>();
    private List<Contact> contacts = new ArrayList<>();

    @Test
    void testGetAllAccountsStatus() throws Exception {
        Account account1 = new Account(1,"Atlassian","301 E Evelyn Ave",null,"Mountain View","CA","94041","United States");
        Account account2 = new Account(2,"Apple","One Apple Park Way",null,"Cupertino","CA","95014","United States");
        accounts.add(account1);
        accounts.add(account2);
        when(repository.findAll()).thenReturn(accounts);
        mvc.perform(get("/accounts").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void testGetAllAccountsResponseContent() throws Exception {
        Account account1 = new Account(1,"Atlassian","301 E Evelyn Ave",null,"Mountain View","CA","94041","United States");
        Account account2 = new Account(2,"Apple","One Apple Park Way",null,"Cupertino","CA","95014","United States");
        accounts.add(account1);
        accounts.add(account2);
        when(repository.findAll()).thenReturn(accounts);
        mvc.perform(get("/accounts").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].companyName", is("Atlassian")));
    }

    @Test
    void testGetAllContactsForAccountById() throws Exception {
        Account account1 = new Account(1,"Atlassian","301 E Evelyn Ave",null,"Mountain View","CA","94041","United States");
        Contact contact1 = new Contact(1,"Lily Hamilton","lily.hamilton@atlassianmarketinginterview.com","5 San Pablo Lane","Suite 7","Plymouth","MA","02360","United States");
        Contact contact2 = new Contact(2,"Ryan Ferguson","ryan.ferguson@atlassianmarketinginterview.com","261 Locust Street",null,"Noblesville","IN","46060","United States");
        contacts.add(contact1);
        contacts.add(contact2);
        account1.setContacts(contacts);
        when(repository.findById(anyInt())).thenReturn(java.util.Optional.of(account1));
        mvc.perform(get("/accounts/1/contacts").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].name", is("Lily Hamilton")));
    }

    @Test
    void testGetAccountById() throws Exception {
        Account account1 = new Account(1,"Atlassian","301 E Evelyn Ave",null,"Mountain View","CA","94041","United States");
        when(repository.findById(anyInt())).thenReturn(java.util.Optional.of(account1));
        mvc.perform(get("/accounts/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.companyName", is("Atlassian")));
    }

    @Test
    void testAddAccount() throws Exception {
        Account account1 = new Account(1,"Atlassian","301 E Evelyn Ave",null,"Mountain View","CA","94041","United States");
        when(repository.save(any(Account.class))).thenReturn(account1);
        MockHttpServletResponse response = mvc.perform(post("/accounts").contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJson(account1))).andReturn().getResponse();
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testUpdateAccount() throws Exception {
        Account account1 = new Account(1,"Atlassian","301 E Evelyn Ave",null,"Mountain View","CA","94041","United States");
        when(repository.findById(anyInt())).thenReturn(java.util.Optional.of(account1));
        when(repository.save(any(Account.class))).thenReturn(account1);
        MockHttpServletResponse response = mvc.perform(put("/accounts/1").contentType(MediaType.APPLICATION_JSON)
            .content(JsonUtil.toJson(account1))).andReturn().getResponse();
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }
}