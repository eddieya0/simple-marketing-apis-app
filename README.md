## Atlassian Code Exercise - Simple Marketing APIs App

### Prerequisites

| Name | Version |
|-----------|-----------|
| Java | 8 |
| Maven | 3.3+ | 

For more information, take a look at [here](https://docs.spring.io/spring-boot/docs/current/reference/html/getting-started.html).
### Server Start Up Instruction
1. Make sure you have the compatible Java and Maven installed in your machine, if not follow the link above;
![Java and Maven version screenshot](src/main/resources/static/screenshot1.png "Java and Maven version screenshot")
2. Clone the repository to your local machine;
3. If you don't have MySQL database installed on your machine, download and install MySQL for your version of macOS [here](https://dev.mysql.com/doc/mysql-getting-started/en/) (mine is mysql-8.0.20-macos10.15-x86_64.dmg);
4. Start MySQL server from terminal or System Preferences;
5. (Optional) Download and install MySQLWorkbench (for better visualization of the DB) [here](https://dev.mysql.com/downloads/workbench/) (mine is mysql-workbench-community-8.0.20-macos-x86_64.dmg);
6. Create database, tables and load initial data in MySQL by running the init script, [atlassianmarketingDBInitScript.sql](atlassianmarketingDBInitScript.sql);
![init SQL script run screenshot](src/main/resources/static/screenshot3.png "init SQL script run screenshot")
7. Review and change DB configurations (username, password) in [application.properties](src/main/resources/application.properties) if necessary;
8. In your terminal, CD to the project directory and run:<br/>
`mvn spring-boot:run`
9. Verify maven run result and make sure Spring Boot server can start without issues.
![Spring Boot server startup screenshot](src/main/resources/static/screenshot2.png "Spring Boot server startup screenshot")


### Tech Stack
* Spring Boot
* MySQL
* JPA and Hibernate

### Milestones Completion
- [x] Can the server be started using the documentation?<br/>
See above.
- [x] Does the application connect to a database?<br/>
![New account created in database screenshot](src/main/resources/static/screenshot14.png "New account created in database screenshot")
- [x] Does the account endpoint handle GET/POST/PUT requests?<br/>
![GET account endpoint screenshot](src/main/resources/static/screenshot4.png "GET account endpoint screenshot")
![GET all accounts endpoint screenshot](src/main/resources/static/screenshot10.png "GET all accounts endpoint screenshot")
![POST account endpoint screenshot](src/main/resources/static/screenshot9.png "POST account endpoint screenshot")
![PUT account endpoint screenshot](src/main/resources/static/screenshot8.png "PUT account endpoint screenshot")
- [x] Does the contact endpoint handle GET/POST/PUT requests?<br/>
![GET contact endpoint screenshot](src/main/resources/static/screenshot7.png "GET contact endpoint screenshot")
![POST contact endpoint screenshot](src/main/resources/static/screenshot13.png "POST contact endpoint screenshot")
![PUT contact endpoint screenshot](src/main/resources/static/screenshot12.png "PUT contact endpoint screenshot")
- [x] Can we associate a contact with an account?<br/>
![Associate contact with account endpoint screenshot](src/main/resources/static/screenshot6.png "Associate contact with account endpoint screenshot")
- [x] Can we get all contacts for an account?<br/>
![Get all contacts for account endpoint screenshot](src/main/resources/static/screenshot5.png "Get all contacts for account endpoint screenshot")
- [x] Have test cases been written?<br/>
![Test cases run screenshot](src/main/resources/static/screenshot11.png "Test cases run screenshot") 